#!/bin/sh

set -e
cd debian/tests

TEST_FAILURE=0
SECRET='some_pointless_secret'
SALT='some_salt'
TIME_COST=3
MEM_COST=12
PARALLELISM=1
HASH_LEN=32
RESULT_EXPECTED='$argon2i$v=19$m=4096,t=3,p=1$c29tZV9zYWx0$DoOfpNEIqvnyPJc9vRrPLLUcQUp9d3v2y3E7a3niKvI'

RESULT_CLI=`echo -n ${SECRET} | argon2 ${SALT} -i -e \
	-t ${TIME_COST} \
	-m ${MEM_COST} \
	-p ${PARALLELISM} \
	-l ${HASH_LEN}`
RESULT_LIB=`./hash-a-secret.py ${SECRET} ${SALT} \
	${TIME_COST} \
	${MEM_COST} \
	${PARALLELISM} \
	${HASH_LEN}`

echo "We expected:  ${RESULT_EXPECTED}"
echo "Cli returned: ${RESULT_CLI}"
echo "Lib returned: ${RESULT_LIB}"

if [ ${RESULT_CLI} != ${RESULT_EXPECTED} ]; then
	echo 'The CLI call did not return the expected result!'
	TEST_FAILURE=1
fi
if [ ${RESULT_LIB} != ${RESULT_EXPECTED} ]; then
	echo 'The python script using libargon2 did not return the expected result!'
	TEST_FAILURE=1
fi

exit ${TEST_FAILURE}
